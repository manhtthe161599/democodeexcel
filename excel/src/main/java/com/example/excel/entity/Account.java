package com.example.excel.entity;



import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    @Column(name = "username")
    private String Username;

    @Column(name = "password")
    private String Password;


    @Column(name = "location")
    private String Location;
    @Column(name = "phonenumber")
    private int Phonenumber;
    @Column(name = "dateofbirth")
    private LocalDate Dateofbirth;




    public Account() {
    }

    public Account(String username, String password, String location, int phonenumber, LocalDate dateofbirth) {
        Username = username;
        Password = password;
        Location = location;
        Phonenumber = phonenumber;
        Dateofbirth = dateofbirth;
    }
    public Account(int id, String username, String password, String location, int phonenumber, LocalDate dateofbirth) {
        Id = id;
        Username = username;
        Password = password;
        Location = location;
        Phonenumber = phonenumber;
        Dateofbirth = dateofbirth;
    }





    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public int getPhonenumber() {
        return Phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        Phonenumber = phonenumber;
    }

    public LocalDate getDateofbirth() {
        return Dateofbirth;
    }

    public void setDateofbirth(LocalDate dateofbirth) {
        Dateofbirth = dateofbirth;
    }

    @Override
    public String toString() {
        return "account{" +
                "Id=" + Id +
                ", Username='" + Username + '\'' +
                ", Password='" + Password + '\'' +
                ", Location='" + Location + '\'' +
                ", Phonenumber=" + Phonenumber +
                ", Dateofbirth='" + Dateofbirth + '\'' +
                '}';
    }


    public void updateAccountInfo(Account newAccountInfo) {
        if (newAccountInfo.getUsername() != null) {
            this.setUsername(newAccountInfo.getUsername());
        }
        if (newAccountInfo.getPassword() != null) {
            this.setPassword(newAccountInfo.getPassword());
        }
        if (newAccountInfo.getLocation() != null) {
            this.setLocation(newAccountInfo.getLocation());
        }
        if (newAccountInfo.getPhonenumber() != 0) {
            this.setPhonenumber(newAccountInfo.getPhonenumber());
        }
        if (newAccountInfo.getDateofbirth() != null) {
            this.setDateofbirth(newAccountInfo.getDateofbirth());
        }

    }


}



