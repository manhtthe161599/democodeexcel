//package com.example.excel.common;
//
//import com.example.excel.entity.ValidationError;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.util.List;
//
//@RestController
//public class ExcelController {
//
//    @Autowired
//    private ExcelService excelService;
//
//    @GetMapping("/createExcel")
//    public ResponseEntity<String> createExcel() {
//        String filePath = excelService.createExcelFile();
//        if (filePath != null) {
//            return ResponseEntity.ok("Excel file created successfully at: " + filePath);
//        } else {
//            return ResponseEntity.status(500).body("Error creating Excel file.");
//        }
//    }
//
//
//
//    @PostMapping("/addAccountsFromExcel")
//    public ResponseEntity<String> addAccountsFromExcel(@RequestParam("file") MultipartFile file) {
//        String result = excelService.addAccountsFromExcel(file);
//        if (result.contains("successfully")) {
//            return ResponseEntity.ok(result);
//        } else {
//            return ResponseEntity.status(500).body(result);
//        }
//    }
//
//
//}
//


package com.example.excel.controller;


import com.example.excel.service.ExcelService;
import com.example.excel.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ExcelController {

    @Autowired
    private ExcelService excelService;

    @GetMapping("/createExcel")
    public ResponseEntity<String> createExcel() {
        String filePath = excelService.createExcelFile();
        if (filePath != null) {
            return ResponseEntity.ok("Excel file created successfully at: " + filePath);
        } else {
            return ResponseEntity.status(500).body("Error creating Excel file.");
        }
    }




    @PostMapping("/addAccountsFromExcel")
    public ResponseEntity<Object> addAccountsFromExcel(@RequestParam("file") MultipartFile file) {
        ResponseEntity<Object> response = excelService.addAccountsFromExcel(file);

        return response;
    }


    //delete
    @DeleteMapping("/deleteAccount/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable int id) {
        String result = excelService.deleteAccount(id);
        return ResponseEntity.ok(result);
    }



    //update
    @PutMapping("/updateAccount/{id}")
    public ResponseEntity<String> updateAccount(@PathVariable int id, @RequestBody Account newAccountInfo) {
        String result = excelService.updateAccount(id, newAccountInfo);
        return ResponseEntity.ok(result);
    }




}

