package com.example.excel.service;

import com.example.excel.repository.AccountRepository;
import com.example.excel.entity.Account;

import com.example.excel.repository.DuplicateError;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class ExcelService {

    @Autowired
    private AccountRepository accountRepository;


    // in danh sach account ra file excel
    public String createExcelFile() {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Account Data");
            List<Account> accounts = accountRepository.findAll();

            int rowNumber = 0;

            for (Account acc : accounts) {
                Row row = sheet.createRow(rowNumber++);
                row.createCell(0).setCellValue(acc.getId());
                row.createCell(1).setCellValue(acc.getUsername());
                row.createCell(2).setCellValue(acc.getPassword());
                row.createCell(3).setCellValue(acc.getLocation());
                row.createCell(4).setCellValue(acc.getPhonenumber());
                row.createCell(5).setCellValue(acc.getDateofbirth().toString());
            }

            String filePath = "account_data.xlsx";
            try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
                workbook.write(outputStream);
            }

            return filePath;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // =================================================================================================================

    //***************   ADD account thong qua file excel


    private static final String EMPTY_STRING = "";

    private static DuplicateError createDuplicateError(String cellPosition, String column, String data, int rowNumber, int columnNumber, String errorDescription) {
        DuplicateError error = new DuplicateError();
        error.setCellPosition(cellPosition);
        error.setColumn(column);
        error.setData(data);
        error.setRow(rowNumber);
        error.setColumnNumber(columnNumber);
        error.setErrorDescription(errorDescription);
        return error;
    }


    public ResponseEntity<Object> addAccountsFromExcel(MultipartFile file) {
        List<DuplicateError> duplicateErrors = new ArrayList<>();
        List<Account> accountsToAdd = new ArrayList<>();
        try (Workbook workbook = new XSSFWorkbook(file.getInputStream())) {
            Sheet sheet = workbook.getSheetAt(0);

            List<String> existingUsernames = accountRepository.findAllUsernames();

            for (Row row : sheet) {
                if (row.getRowNum() == 0) {
                    continue;
                }

                Cell usernameCell = row.getCell(0);
                String username = "";
                String cellPosition = "";

                //========== Kiểm tra điều kiện isEmpty của username
                if (usernameCell != null) {
                    username = usernameCell.getStringCellValue().trim();

                }else {
                    CellReference cellReference = new CellReference(row.getRowNum(), 0);
                    cellPosition = cellReference.formatAsString();

                    DuplicateError error = createDuplicateError(cellPosition, "username", EMPTY_STRING, row.getRowNum() + 1, 1, "Username không được để trống");
                    duplicateErrors.add(error);
                }



                //=========== kiểm tra điều kiện bị trùng của username
                if (existingUsernames.contains(username)) {
                    CellReference cellReference = new CellReference(row.getRowNum(), usernameCell.getColumnIndex());
                    cellPosition = cellReference.formatAsString();

                    DuplicateError error = createDuplicateError(cellPosition, "username", username, row.getRowNum() + 1, usernameCell.getColumnIndex() + 1, "Username bị trùng");
                    duplicateErrors.add(error);

                } else {
                    // ==============Kiểm tra điều kiện isEmpty của password

                    Cell passwordCell = row.getCell(1);
                    String password = "";
                    if (passwordCell != null) {
                        password = passwordCell.getStringCellValue().trim();
                    }else {
                        CellReference cellReference = new CellReference(row.getRowNum(), 1);
                        cellPosition = cellReference.formatAsString();

                        DuplicateError error = createDuplicateError(cellPosition, "password", EMPTY_STRING, row.getRowNum() + 1, 1, "Password không được để trống");
                        duplicateErrors.add(error);
                    }

                    Cell locationCell = row.getCell(2);
                    String location = "";
                    if (locationCell != null) {
                        location = locationCell.getStringCellValue().trim();
                    }
                    Cell phoneNumberCell = row.getCell(3);
                    int phoneNumber = 0;
                    if (phoneNumberCell != null) {
                        phoneNumber = (int) phoneNumberCell.getNumericCellValue();
                    }



                    // ===============Kiểm tra điều kiện của date of birth
                    Cell dobCell = row.getCell(4);
                    LocalDate dateOfBirth = null;
                    if (dobCell != null) {
                        int cellType = dobCell.getCellType();
                        if (cellType == Cell.CELL_TYPE_STRING) {
                            String dobString = dobCell.getStringCellValue().trim();

                            try {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                dateOfBirth = LocalDate.parse(dobString, formatter);
                            } catch (DateTimeParseException e) {
                                CellReference cellReference = new CellReference(row.getRowNum(), dobCell.getColumnIndex());
                                cellPosition = cellReference.formatAsString();
                                DuplicateError error = createDuplicateError(cellPosition, "dateOfBirth", dobString, row.getRowNum() + 1, dobCell.getColumnIndex() + 1, "Ngày sinh có định dạng không hợp lệ");
                                duplicateErrors.add(error);
                            }
                        } else if (cellType == Cell.CELL_TYPE_NUMERIC) {
                            if (DateUtil.isCellDateFormatted(dobCell)) {
                                Date dateOfBirthDate = dobCell.getDateCellValue();
                                dateOfBirth = dateOfBirthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            }
                        }
                    }

                    Account account = new Account(username, password, location, phoneNumber, dateOfBirth);
                    accountsToAdd.add(account);
                }
            }


            if (!duplicateErrors.isEmpty()) {
                return ResponseEntity.badRequest().body(duplicateErrors);
            }

            if (!accountsToAdd.isEmpty()) {
                accountRepository.saveAll(accountsToAdd);
                return ResponseEntity.ok("Add account thành công");
            }
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            e.printStackTrace();
            DuplicateError fileReadError = createDuplicateError("","","",-1,-1,"Lỗi trong quá trình đọc file");
            duplicateErrors.add(fileReadError);
            return ResponseEntity.badRequest().body(duplicateErrors);
        }


    }

// =================================================================================================================

    // delete
    public String deleteAccount(int accountId) {
        Account accountToDelete = accountRepository.findById(accountId);
        if (accountToDelete == null) {
            return "Account có ID " + accountId + " ko tìm thấy!!";
        } else {
            accountRepository.delete(accountToDelete);
            return "Account có ID " + accountId + " delete thành công";
        }
    }

// =================================================================================================================

    //update

    public String updateAccount(int accountId, Account newAccountInfo) {
        Account existingAccount = accountRepository.findById(accountId);
        if (existingAccount == null) {
            return "Account có ID " + accountId + " ko tìm thấy!!";
        } else {
            existingAccount.updateAccountInfo(newAccountInfo);
            accountRepository.save(existingAccount);
            return "Account có ID: " + accountId + " update thành công!!";
        }
    }



}

