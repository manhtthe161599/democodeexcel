package com.example.excel.repository;



public class ExcelUploadResponse {
    private String message;

    public ExcelUploadResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}




