package com.example.excel.repository;

import com.example.excel.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
    @Query("SELECT a.Username FROM Account a")
    List<String> findAllUsernames();

    void deleteById(int id);

    Account findById(int id);

    @Query("SELECT MAX(a.id) FROM Account a")
    Integer findMaxId();
}



