package com.example.excel.repository;

public class DuplicateError {
    private String column;
    private String data;
    private int row;
    private int columnNumber;
    private String errorDescription;
    private String cellPosition;

    // Constructors, getters, setters


    public DuplicateError() {
    }

    public DuplicateError(String column, String data, int row, int columnNumber, String errorDescription) {
        this.column = column;
        this.data = data;
        this.row = row;
        this.columnNumber = columnNumber;
        this.errorDescription = errorDescription;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public void setColumnNumber(int columnNumber) {
        this.columnNumber = columnNumber;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public void setCellPosition(String cellPosition) {
        this.cellPosition = cellPosition;
    }

    public String getCellPosition() {
        return cellPosition;
    }
}

